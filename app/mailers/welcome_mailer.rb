class WelcomeMailer < ActionMailer::Base
  default from: "flatrent.team@gmail.com"
  
  def welcome(recipient)
    @recipient = recipient
    mail(to: recipient.email,
         subject: "Witamy na FlatRent!")
  end
end