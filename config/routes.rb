FlatRent::Application.routes.draw do
  resources :photos

  resources :offers

  get 'users/ranking'
  resources :events do
    collection do
         get :likeit
     end
  end

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  mount Commontator::Engine => '/commontator'
end